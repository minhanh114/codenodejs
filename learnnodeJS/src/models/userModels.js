const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const validator = require('validator') //verify dau vao data
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
      name: {
        type: String,
        required: true,
        trim: true
      },
      email: {
        type: String,
        unique: true, // la duy nhat
        required: true,
        trim: true, // loai bo khoang trang o dau va cuoi chuoi
        lowercase: true,
        // validate(value) {
        //     if(!validator.isEmail(value))
        //         throw new Error("Email is invalid")
        // }
      },
      password: {
        type: String,
        required: true,
        trim: true,
        minlength: 7,
        // validate(value) {
        //     if(value.toLowerCase().includes('password')){ //khong lam thay doi chuoi goc ban dau va password khong chua kitu trong ''
        //         throw new Error("Password cannot contain 'password'")
        //     }
        // }
      },
      tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
      tokenn: {
        type: String
  }
  },
  {
    timestamps: true
}
)
//ham kiem tra thong tin dang nhap
userSchema.statics.findByCredentials = async (email,password) =>{
    const user = await User.findOne({ email })
    if(!user){
        throw new Error('Login failed by email')
    }
    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        throw new Error('Login failed by password')
    }
    return user
}
// chuyen (bam) mat khau nho bcrypt va luu xuong DB
//hashing password
//salt duoc gan tu dong truoc pass client nhap
userSchema.pre('save', async function (next){
  const user = this
  const salt = await bcrypt.genSalt()
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, salt)
  }
  next()
})
//cai dat auth de show token (json)
userSchema.methods.generateAuthToken = async function () {
  const user = this
  const token = jwt.sign({ _id: user._id.toString() }, 'thisismynewcourse')//ma thong bao xac thuc

  user.tokens = user.tokens.concat({ token })
  await user.save()
  return token
}
//hiding private
userSchema.methods.toJSON = function(){
  const user = this
  const userObject = user.toObject()
  delete userObject.password
  delete userObject.tokens
  return userObject
}
// Delete user tasks when user is removed
userSchema.pre('remove', async function (next) {
  const user = this
  await Task.deleteMany({ owner: user._id })
  next()
})
const User = mongoose.model('User', userSchema)

module.exports = User