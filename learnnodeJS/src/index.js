const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const morgan = require('morgan')
const cors = require('cors')
const router = require('./routes')

// const mongo = require('mongodb');
// const mc = mongo.MongoClient;
// const url = ""; 
// mc.connect( url,  function (err, db) { 
//     if(err) throw err; 
//     console.log("Connect sucessful");
// });
dotenv.config()
const app = express()
//middLware
app.use(cors()) //bat ki ai cung co the truy cap cap vao api nay
app.use(morgan('dev'))
app.use(express.json()) //du lieu kieu json
app.use(express.urlencoded()); //du lieu kieu form
//database
const URI = process.env.MONGODB_URL;
//console.log(process.env.MONGODB_URL);
mongoose.connect(URI, {
    autoIndex: false
  }, (err) => {
    if(err) throw err;
    console.log('Connection sucessful.')
  })
//Routes
// cac phuong thuc chuc nang http thuong gap
app.get('/',(req,res)=>{
    res.json({msg: "Hello world"})
})
app.use(router)
//
// app.get('/api/product',(req,res)=>{
//     res.json({msg: "Get all products"})
// })
//truoc dau ":" thi se duoc hieu la tham so luu vao req.params
// app.get('/api/product/:id',(req,res)=>{
//     console.log(req.params)
//     res.json({msg: `Get one product by id ${req.params.id}`})
// })
// app.post('/api/products/',(req,res)=>{
//     res.json({msg: "add new product"})
// })
// app.put('/api/products/:id',(req,res)=>{
//     res.json({msg: `update product by id ${req.params.id} `})
// })
// app.put('/api/products/:id/:pid',(req,res)=>{
//     res.json({msg: `update product by id ${req.params.id} `})
// })
// app.delete('/api/products/:id',(req,res)=>{
//     res.json({msg: `delete a product by id ${req.params.id} `})
// })
//start server listen
const port = process.env.POST || 3000;
app.listen(port,()=>{
    console.log(`Express is listening on port ${port}`)
})