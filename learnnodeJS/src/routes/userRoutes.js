const express = require('express')
const {auth} = require('../middleware/authorization')
const userCtl = require('../controllers/userCtrlr')
const {userValidation,loginValidation,updateValidation} = require('../middleware/validateUser')

const router = express.Router()
router.get('/api/users',userCtl.getUsers)

router.get('/api/user/reset-password',userCtl.resetPass)

router.get('/api/user/:id',auth,userCtl.getUsersID)

router.post('/api/user/',userValidation,userCtl.addUser) //tao  user

router.patch('/api/user/:id',updateValidation,userCtl.updateUser)

router.delete('/api/user/:id',userCtl.deleteUser)

router.post('/api/user/login',loginValidation,userCtl.checkUser) // login

router.post('/api/user/logout',auth,userCtl.logoutUser)

router.post('/api/user/logoutAll',auth,userCtl.logoutUserAll)

router.post('/api/user/forgetpass',userCtl.forgetPass)



module.exports = router