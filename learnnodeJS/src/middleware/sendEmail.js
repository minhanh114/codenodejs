const nodemailer = require('nodemailer') //config gui nhan email 
const dotenv = require('dotenv')
dotenv.config()
const sendEmail = async(name, email, tokenn) =>{
    try{
        const transporter = nodemailer.createTransport({
            host: process.env.HOST,
            service: process.env.SERVICE,
            port: 587,
            secure: false,
            requireTLS: true,
                auth:{
                    user: process.env.USER,
                    pass: process.env.PASS
                }
        })
        const mailOptions = {
            from: process.env.USER,
            to: email,  //email
            subject: "For reset password",
            html: '<p> Hi ' +name+ ', Please copy the link and < a href="http://localhost:3000/api/user/reset-password?tokenn='+tokenn+'"> reset your password </a>'
        }
        transporter.sendMail(mailOptions,function(error, info){
            if(error){
                console.log(error)
            }else{
                console.log("Mail has been sent: ",info.response)
            }
        })
    }catch(error){
        console.log(error)
        console.log("Email not sent")
    }
}
module.exports = sendEmail