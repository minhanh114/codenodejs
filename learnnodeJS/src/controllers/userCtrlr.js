const Joi = require('joi')
const User = require('../models/userModels')
const sendEmail  = require('../middleware/sendEmail')
const userValidation = require('../middleware/validateUser')
const randomstring = require('randomstring')
const bcrypt = require('bcryptjs')
//const Tonken = require('../models/tokens')
const dotenv = require('dotenv')
const { send } = require('process')
dotenv.config()
const userCtl = {
    getUsers: async (req, res) =>{
        try{
            const user = await User.find()
            return res.status(200).json(user)
        }catch(err){
            return res.status(500).json({msg: err.message})
        }
    },
    getUsersID: async (req, res) =>{
        try{
            const user = await User.findById(req.params.id)
            if(!user)
                return res.status(404).json({msg:"Not found"})
            return res.status(200).json(user)
        }catch(err){
            return res.status(500).json({msg: err.message})
        }
    },
    addUser: async (req,res)=>{
        try{
            const existingUser = await User.findOne({
                email: req.body.email,
            }).lean(true);
            if (existingUser) {
                return res.status(403).json("User Already Exists",{msg: existingUser.message});
            } else {
            //console.log({body: req.body})//body la ttin khach hang nhap gui len de server luu
            const newuser = new User(req.body)//loc bien dich lai du lieu tu khach hang vao db va tra lai cho server
            //du lieu sau khi server xu ly va return cho khach hang
            //console.log({newuser})
            await newuser.save()//luu vao co so du lieu
            const token = await newuser.generateAuthToken()
                 return res.status(200).json({ newuser,token })
        }
        } catch (err){
        return res.status(500).json({msg: err.message})
        }    
    },
    //kiem tra khi nguoi dung dang nhap
    checkUser: async (req,res)=>{
        try{
            const user =  await User.findByCredentials(req.body.email, req.body.password)
            const token = await user.generateAuthToken()
                return res.status(200).json({user, token})
            //res.send({ user, token })
        } catch (err){
        return res.status(500).json({msg: err.message})
        }    
    },
    logoutUser: async (req,res)=>{
        try {
            req.user.tokens = req.user.tokens.filter((token) => {
                return token.token !== req.token
            })
            await req.user.save()
            res.send('Logout successful !')
            
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    logoutUserAll: async (req,res)=>{
        try {
            req.user.tokens = [] // token thanh mang trong
            await req.user.save() //luu cap nhap lai
            res.send('Logout all sessions !')
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    //can xem lai
    updateUser: async (req,res)=>{
        try{
            const existingUser = await User.findOne({
                email: req.body.email,
            }).lean(true);
            if (existingUser) {
                return res.status(403).json("User Already Exists",{msg: existingUser.message});
            } else {
            const {name,email, password}=req.body
            const user = await User.findByIdAndUpdate(req.params.id,{
                name,email, password},{new: true}//de tra ve gia tri moi
            )
                if (! user)
                    return res.status(404).json({msg:"Not found"})
                return res.status(200).json(user)
            }
        } catch (err){
        return res.status(500).json({msg: err.message})
        }
    },
    deleteUser: async (req, res) => {
        try {
          
          const user = await User.findByIdAndDelete(req.params.id)
    
          if(!user) 
                return res.status(404).json({msg: 'Not found'})
          return res.status(200).json({msg: 'Delete Success'})
        } catch (err) {
          return res.status(500).json({msg: err.message})
        }
    },
    forgetPass: async (req, res) => {
        try {
          const schema = Joi.object({email: Joi.string().email().required()})
          const {error} = schema.validate(req.body)
          if(error) 
                return res.status(404).json(error.details[0].message)
          const email = req.body.email
          const user = await User.findOne({email: email})

          if(user){
                const randomString = randomstring.generate()
                const data = await User.updateOne({email: email},{$set:{tokenn:randomString}})
                sendEmail(user.name,user.email,randomString)
                return res.status(200).send({success:true, msg:"Please check your inbox of mail"})
          }else{
            return res.status(500).send({success:false, msg:"This email doesn't exist"})
        }
        } catch (err) {
            res.send("Failed")
            console.log(err);        
        }
    },
    resetPass: async (req, res) => {
        try {
            const schema = Joi.object({ password: Joi.string().required() });
            const { error } = schema.validate(req.body);
            if (error) 
                return res.status(400).send(error.details[0].message);
            const tokenn = req.query.tokenn
            const tokenData =  await User.findOne({tokenn:tokenn})
            if(tokenData){
                const salt = await bcrypt.genSalt()
                const newpassword= await bcrypt.hash(req.body.password, salt)
                
                const user = await User.findByIdAndUpdate({ _id:tokenData._id },{$set:{ password:newpassword, tokenn:''}},{new : true})
                
                return res.status(200).send({success:true, msg:"User password reset successful",data:user})
            }else{
                return res.status(200).send({success:true, msg:"This link has been expried"})
            }

        } catch (error) {
            return res.status(400).send({success:false, msg:error.message})
            console.log(error);
        }
    }
}
module.exports = userCtl