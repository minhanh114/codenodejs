//gom xử lý các hàm callback để làm gọn ở folder routes
const Products = require('../models/productModels')

const productCtl = {
//     getProducts: async (req,res)=>{
//         res.json({msg: "Get all products"})
// },
    getproducts: async (req,res)=>{
       try{
            console.log(req.query)
            const page = req.query.page *1 || 1         //*1 de cover string nguoi dung gui thanh number,  de mac dinh la trang 1
            const limit = req.query.limit *1 || 5
            const skip = limit*(page -1)
            const sort = req.query.sort *1 || '-createdAt' //'-price' // sap xep giam dan theo ngay khoi tao
            const product = await Products.find().limit(limit).skip(skip).sort(sort)
            //const product = await Products.find()// showAll
            //const product = await Products.find().limit(5) //loc chi lay 5 sp dau tien
            //const product = await Products.find().limit(5).skip(5) //bo qua 5 sp dau tien lay 5 sp cua trang t2
            //const product = await Products.find().limit(5).skip(10) //bo qua 5 sp dau tien lay 5 sp cua trang t2

                return res.status(200).json(product)

       } catch (err){
        return res.status(500).json({msg: err.message})
       }
    },
    getproductsID: async (req,res)=>{
        try{
            const product = await Products.findById(req.params.id)
                if (! product)
                    return res.status(404).json({msg:"Not found"})
                return res.status(200).json(product)
        } catch (err){
        return res.status(500).json({msg: err.message})
        }
    },
    addProducts: async (req,res)=>{
        try{
            const {title,image, description,category, price}=req.body//rang buoc khi nguoi dung nhap khac du lieu thi server ko nhan
            console.log({body: req.body})//body la ttin khach hang nhap gui len de server luu
            const testproduct = new Products(req.body)//loc bien dich lai du lieu tu khach hang vao db va tra lai cho server
            console.log({testproduct})//du lieu sau khi server xu ly va return cho khach hang
            await testproduct.save()//luu vao co so du lieu
                 return res.status(200).json(testproduct)

        } catch (err){
        return res.status(500).json({msg: err.message})
        }    
    },
    updateProducts: async (req,res)=>{
        try{
            const {title,image, description,category, price}=req.body
            const product = await Products.findByIdAndUpdate(req.params.id,{
                title,image, description,category, price},{new: true}//de tra ve gia tri moi
            )
                if (! product)
                    return res.status(404).json({msg:"Not found"})
                return res.status(200).json(product)
        } catch (err){
        return res.status(500).json({msg: err.message})
        }
    },
    deleteProduct: async (req, res) => {
        try {
          
          const product = await Products.findByIdAndDelete(req.params.id)
    
          if(!product) 
            return res.status(404).json({msg: 'Not found'})
    
          return res.status(200).json({msg: 'Delete Success'})
        } catch (err) {
          return res.status(500).json({msg: err.message})
        }
    }
}
//export default productCtl
module.exports = productCtl