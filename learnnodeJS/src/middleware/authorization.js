const User =  require('../models/userModels')
const jwt = require('jsonwebtoken') 
//trung gian, gphep xac thuc, cap quyen cho user sau khi login, tra ve token dang nhap
module.exports.auth = async (req, res, next) =>{
    try{
        const token = req.header('Authorization').replace('Bearer ', '')
        //console.log(token)
        const decoded = jwt.verify(token, 'thisismynewcourse')
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })

        if (!user){
            throw new Error({ error: 'failed' })
        }
        req.token = token
        req.user = user
        next()
    } catch(e){
        res.status(401).send({ error: 'Please authenticate' })
    }
}
 