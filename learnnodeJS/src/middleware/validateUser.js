const Joi = require("joi");

const userSchema = Joi.object({
  name : Joi.string().alphanum().required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(3).max(10).required(),
});

const userValidation = async (req, res, next) => {
	const payload = {
		name: req.body.name,
		email: req.body.email,
		password: req.body.password
	};
	const { error } = userSchema.validate(payload);
	if (error) {
		return res.status(406).json({msg: error.message})
	} else {
		next();
	}
};

const loginSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(3).max(10).required(),
});
const loginValidation = async (req, res, next) => {
	const payload = {
		email: req.body.email,
		password: req.body.password
	};
	const { error } = loginSchema.validate(payload);
	if (error) {
		return res.status(406).json({msg: error.message})
	} else {
		next();
	}
};

const updateSchema = Joi.object({
  name : Joi.string().alphanum(),
  email: Joi.string().email(),
  password: Joi.string().min(3).max(10),
});
const updateValidation = async (req, res, next) => {
	const payload = {
    	name: req.body.name,
		email: req.body.email,
		password: req.body.password
	};
	const { error } = updateSchema.validate(payload);
	if (error) {
		return res.status(406).json({msg: error.message})
	} else {
		next();
	}
};

module.exports = {userValidation,loginValidation,updateValidation};