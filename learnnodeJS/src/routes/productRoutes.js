//gom lại tránh các chức năng quá nhiều ở hàm index
const express = require('express')
const {checkProductdata} = require('../middleware/validate')
const producCtl = require('../controllers/productCtrlr')
const {auth} = require('../middleware/authorization')

const router = express.Router()
//router.get('/api/product',producCtl.getProducts)
router.get('/api/products',auth,producCtl.getproducts)
// router.get('/api/product/:id',(req,res)=>{
//     console.log(req.params)
//     res.json({msg: `Get one product by id ${req.params.id}`})
// })
router.get('/api/product/:id',auth,producCtl.getproductsID)
router.post('/api/products/',auth,checkProductdata,producCtl.addProducts)
router.put('/api/products/:id',auth,checkProductdata,producCtl.updateProducts)
// // app.put('/api/products/:id/:pid',(req,res)=>{
// //     res.json({msg: `update product by id ${req.params.id} `})
// // })
router.delete('/api/products/:id',auth,producCtl.deleteProduct)

//export default router;
module.exports = router