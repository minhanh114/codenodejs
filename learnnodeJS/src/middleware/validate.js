// chan truong hop khi ko nhap du cac truong du lieu bi quay lai cac giai doan truoc mat thoi gian
module.exports.checkProductdata = async(req, res, next) =>{
    const {title,image, description,category, price}=req.body
    const errors = [];
    for(const key in req.body){
        if(!req.body[key]){
            errors.push(`Please add product ${key}`)
        }
    }
    if(errors.length > 0)
        return res.status(401).json({msg: errors})
    next()
}
// const validator = (schema) => (payload) =>
//     schema.validate(payload,{abortEarly: false})
    
// const userSchema = joi.object({
//     email: joi.string().email().required(),
//     password: joi.string().min(7).max(15).required(),
// })
// exports.checkUserdata = validator(userSchema)